import { getListServ } from "./services/serviceSanPhamSale.js";
import { SanPham } from "./models/modelSanPhamSale.js";
import {
  offLoading,
  onLoading,
  renderThongTinSanPham,
  renderCart,
  tinhTongSP,
  tinhTongThanhToan,
  saveLocalStorage,
} from "./controllers/controllerSanPhamSale.js";

//danh sach san pham
let danhSachSP = [];
let cart = [];
const LISTCART = "LISTCART";

//lay dsnv tu localStorage
let dataJson = localStorage.getItem(LISTCART);
let dataRaw = JSON.parse(dataJson);
if (dataRaw) {
  cart = dataRaw;
  renderCart(cart);
  tinhTongSP(cart);
}

//load data SP
let renderListProductsServ = () => {
  onLoading();
  getListServ()
    .then((res) => {
      let danhSachSanPham = res.data.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      danhSachSP = danhSachSanPham;
      renderThongTinSanPham(danhSachSanPham);
      offLoading();
    })
    .catch((err) => {
      Swal.fire("Load danh sách Sản phẩm thất bại!");
      offLoading();
    });
};
renderListProductsServ();

//tim ten san pham
document.getElementById("btnTimSP").onclick = () => {
  onLoading();
  let tenSP = document.getElementById("txtSanPham").value;
  getListServ()
    .then((res) => {
      let timSanPham = res.data.filter((sanPham) => {
        return sanPham.name.toLowerCase().indexOf(tenSP.toLowerCase()) > -1;
      });
      if (timSanPham.length == 0) {
        offLoading();
        return Swal.fire("Không tìm thấy Sản Phẩm!");
      }
      let danhSachSanPham = timSanPham.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      offLoading();
      renderThongTinSanPham(danhSachSanPham);
    })
    .catch((err) => {
      offLoading();
      Swal.fire("Lấy dữ liệu sản phẩm thất bại!");
    });
};

//loc san pham
document.getElementById("loaiSP").onchange = () => {
  onLoading();
  let loaiSP = document.getElementById("loaiSP").value;
  getListServ()
    .then((res) => {
      let timLoaiSanPham = res.data.filter((sanPham) => {
        return sanPham.type.toLowerCase().indexOf(loaiSP.toLowerCase()) > -1;
      });
      if (timLoaiSanPham.length == 0) {
        offLoading();
        return Swal.fire("Không tìm thấy Sản phẩm nào!");
      }
      let danhSachSanPham = timLoaiSanPham.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      offLoading();
      renderThongTinSanPham(danhSachSanPham);
    })
    .catch((err) => {
      offLoading();
      Swal.fire("Lọc sản phẩm thất bại!");
    });
};

///tang giam so luong mua san pham

let qtyChange = (chose, change) => {
  let count = 0;
  if (change == "up") {
    count++;
  } else {
    count--;
  }
  chose.parentElement.classList.add("active");
  let soLuong = document.querySelector(".active #soLuongSP").innerText * 1;
  soLuong += count;
  if (soLuong <= 0) {
    soLuong = 0;
  }
  document.querySelector(".active #soLuongSP").innerText = soLuong;
  chose.parentElement.classList.remove("active");
};
window.qtyChange = qtyChange;

//add to cart

let addToCart = (check, idSP) => {
  //
  // check.parentElement.classList.add("check");
  check.parentElement.classList.add("flagCheck");
  let soLuong = document.querySelector(".flagCheck #soLuongSP").innerText * 1;
  document.querySelector(".flagCheck #soLuongSP").innerText = 0;
  check.parentElement.classList.remove("flagCheck");
  if (soLuong == 0) {
    return Swal.fire("Vui lòng chọn số lượng sản phẩm cần mua!");
    // soLuong = 1;
    // document.querySelector(".flagCheck #soLuongSP").innerText = soLuong;
  }
  //

  let sanPhamEdit = danhSachSP.filter((sanPham) => {
    return sanPham.id == idSP;
  });
  let thongtinSanPhamMua = {
    product: sanPhamEdit[0],
    quantity: soLuong,
  };

  //so sanh san pham da co trong cart
  let timSanPhamInCart = cart.filter((item, index) => {
    if (item.product.id == idSP) {
      let sanPham = cart[index];
      cart.splice(index, 1);
      return sanPham;
    }
  });
  if (timSanPhamInCart == 0) {
    cart.push(thongtinSanPhamMua);
  } else {
    let soLuongSP = thongtinSanPhamMua.quantity + timSanPhamInCart[0].quantity;
    thongtinSanPhamMua.quantity = soLuongSP;
    cart.push(thongtinSanPhamMua);
  }
  tinhTongSP(cart);
  Swal.fire("Thêm sản phẩm vào giỏ hàng thành công!");
  saveLocalStorage(LISTCART, cart);
};
window.addToCart = addToCart;

// button cart
document.getElementById("btnCart").onclick = () => {
  renderCart(cart);
  document.getElementById("tongThanhToan").innerText = tinhTongThanhToan(cart);
};
// thay doi so luong san pham trong Cart
let qtyChangeCart = (chose, change, idSP) => {
  let count = 0;
  if (change == "up") {
    count++;
  } else {
    count--;
  }
  chose.parentElement.classList.add("active");
  let soLuong = document.querySelector(".active #soLuongSP").innerText * 1;
  soLuong += count;
  if (soLuong <= 1) {
    soLuong = 1;
  }
  document.querySelector(".active #soLuongSP").innerText = soLuong;
  chose.parentElement.classList.remove("active");
  cart.filter((item) => {
    if (item.product.id == idSP) {
      item.quantity = soLuong;
    }
  });
  renderCart(cart);
  tinhTongSP(cart);
  saveLocalStorage(LISTCART, cart);
  document.getElementById("tongThanhToan").innerText = tinhTongThanhToan(cart);
};
window.qtyChangeCart = qtyChangeCart;

//xoa tung san pham
let xoaSanPham = (idSP) => {
  cart.filter((item, index) => {
    if (item.product.id == idSP) {
      cart.splice(index, 1);
    }
  });
  renderCart(cart);
  tinhTongSP(cart);
  saveLocalStorage(LISTCART, cart);
  document.getElementById("tongThanhToan").innerText = tinhTongThanhToan(cart);
};
window.xoaSanPham = xoaSanPham;

//clear Cart
document.getElementById("btnClear").onclick = () => {
  cart = [];
  renderCart(cart);
  tinhTongSP(cart);
  saveLocalStorage(LISTCART, cart);
  document.getElementById("tongThanhToan").innerText = 0;
};

//thanh toan san pham
document.getElementById("btnThanhToan").onclick = () => {
  let tongTien = document.getElementById("tongThanhToan").innerText;
  Swal.fire(`Số tiền đã thanh toán thành công là: $ ${tongTien} usd`);
  cart = [];
  renderCart(cart);
  tinhTongSP(cart);
  saveLocalStorage(LISTCART, cart);
  document.getElementById("tongThanhToan").innerText = 0;
};
