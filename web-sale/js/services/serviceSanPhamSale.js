let BASE_URL = "https://633ec04e83f50e9ba3b76077.mockapi.io";
export let getListServ = () => {
  return axios({
    url: `${BASE_URL}/web-sale`,
    method: "GET",
  });
};
